package model.logic_SortingAlgorithms;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.logic.Ordenador;
import model.logic.SortingAlgorithms;

class MergeSortTest {
	
	private String[] strings;
	
	private Integer[] integers;

	//pruebas con Strings
	@Before
	void setUpEscenario1() {
		strings = new String[10];
		strings[0] = "a";
		strings[1] = "c";
		strings[2] = "e";
		strings[3] = "h";
		strings[4] = "z";
		strings[5] = "w";
		strings[6] = "m";
		strings[7] = "n";
		strings[8] = "i";
		strings[9] = "j";
	}

	//Pruebas con integers
	@Before
	void setUpEscenario2() {
		integers = new Integer[10];
		integers[0] = 100;
		integers[1] = 200;
		integers[2] = 40;
		integers[3] = 0;
		integers[4] = 20;
		integers[5] = 1000;
		integers[6] = 45;
		integers[7] = 4;
		integers[8] = 6;
		integers[9] = 9;
	}
	
	//Prueba de ordenamiento ascendente con strings
	@Test
	void testStringsAsc() {
		setUpEscenario1();
		System.out.println("Lista desordenada: "+ Arrays.toString(strings));
		
		Ordenador<String> o = new Ordenador<String>();
		
		o.ordenar(SortingAlgorithms.MERGESORT, true, strings);
		
		System.out.println("Lista ordenada (ascendente): "+ Arrays.toString(strings));
		
		for(int i = 0; i < strings.length-1; i++) {
			assertTrue(strings[i].compareTo(strings[i+1])<0);
		}
		
	}

	//Prueba de ordenamiento descendente con strings
	@Test
	void testStringsDesc() {
		setUpEscenario1();
		System.out.println("Lista desordenada: "+ Arrays.toString(strings));
		
		Ordenador<String> o = new Ordenador<String>();
		
		o.ordenar(SortingAlgorithms.MERGESORT, false, strings);
		
		System.out.println("Lista ordenada (descendente): "+ Arrays.toString(strings));
		
		for(int i = 0; i < strings.length-1; i++) {
			assertTrue(strings[i].compareTo(strings[i+1])>0);
		}
		
	}

	//Prueba de ordenamiento ascendente con integers
	@Test
	void testIntegersAsc() {
		setUpEscenario2();
		System.out.println("Lista desordenada de integers: "+ Arrays.toString(integers));

		Ordenador<Integer> o = new Ordenador<Integer>();
		o.ordenar(SortingAlgorithms.MERGESORT, true, integers);

		for(int i = 0; i < integers.length-1; i++) {
			assertTrue(integers[i] < integers[i+1]);
		}
		
		System.out.println("Lista ordenada de integers (ascendente): "+ Arrays.toString(integers));
	}

	//Prueba de ordenamiento descendente con integers
	@Test
	void testIntegersDesc() {
		setUpEscenario2();
		System.out.println("Lista desordenada de integers: "+ Arrays.toString(integers));

		Ordenador<Integer> o = new Ordenador<Integer>();
		o.ordenar(SortingAlgorithms.MERGESORT, false, integers);

		for(int i = 0; i < integers.length-1; i++) {
			assertTrue(integers[i] > integers[i+1]);
		}
		
		System.out.println("Lista ordenada de integers (descendente): "+ Arrays.toString(integers));
		
	}

}
