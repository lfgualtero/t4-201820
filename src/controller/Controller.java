package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Stack;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadTrips() {
		manager.loadTrips("./data/Divvy_Trips_2017_Q2.csv");

	}
		
	public static int TripsSSize() {
		return manager.getTripsSSize();
	}
	
	public static void mergeSort(VOTrip[] list, boolean asc) {
		 manager.mergeSortTrips(list, asc);
	}
	
	public static void quickSort(VOTrip[] list, boolean asc) {
		 manager.quickSortTrips(list, asc);
	}

	public static VOTrip[] getNTrips (int n) {
		return manager.getNTrips(n);
	}
	
	public static Stack<VOTrip> getTrips () {
		return manager.getTrips();
	}
	
}
