package api;

import model.data_structures.Stack;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	
	
	/**
	* Method to search for the N-service that finished in a station
	* @param stationId station's Id
	* @param n number of  service to search. It can be null.
	*/
	VOTrip customerNumberN (int stationID, int n);


	 VOTrip[] getNTrips(int n);
	
	int getTripsSSize();

	void mergeSortTrips(VOTrip[] elementos, boolean asc);


	void quickSortTrips(VOTrip[] elementos, boolean asc);

	Stack<VOTrip> getTrips();


	
}
