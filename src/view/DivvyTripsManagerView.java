package view;

import java.util.LinkedList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		VOTrip[] nTrips = null;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				Controller.loadTrips();
				
				System.out.println("Tama�o de la pila: " + Controller.TripsSSize());
	
//				for(VOTrip temp : Controller.getTrips()) {
//					System.out.println(temp.toString());
//				}
				break;

			case 2:
				// Requerimiento 2
				System.out.println("Ingrese en n�mero de viajes para la muestra:");
				int numberOfTrips = Integer.parseInt(sc.next());

				nTrips = Controller.getNTrips (numberOfTrips);
				System.out.println("Muestra de " + numberOfTrips + " viajes: ");
				for (VOTrip trip : nTrips) 
				{
					System.out.println( trip );
				}

				System.out.println("Quedan " + Controller.TripsSSize()  + " viajes en la pila principal");
				break;

			case 3:
				System.out.println("Ingrese 1 si quiere ordenar la lista ascendentemente o 0 de lo contrario");
				int respuesta = Integer.parseInt(sc.next());
				boolean asc;
				if(respuesta == 0 || respuesta == 1) {
					asc = respuesta == 1 ? true : false;
					if(nTrips != null) {
						Stopwatch timerX=new Stopwatch();
						Controller.mergeSort(nTrips, asc);
						double timeX= timerX.elapsedTime();
						for (VOTrip trip : nTrips) 
						{
							System.out.println( trip );
						}
						System.out.println( "El tiempo (en milisegundos) que tom� el algoritmo de ordenamiento fue de: " + timeX );
					}
					else {
						System.out.println("Primero debe generar una muestra");
					}
				}
				else {
					System.out.println("Debe ingresar 0 o 1");
					break;				
				}
				break;

			case 4:
				System.out.println("Ingrese 1 si quiere ordenar la lista ascendentemente o 0 de lo contrario");
				
				int respuestaX = Integer.parseInt(sc.next());
				boolean ascX;
				if(respuestaX == 0 || respuestaX == 1) {
					ascX = (respuestaX == 1 ? true : false);
					if(nTrips != null) {
						
						Stopwatch timer=new Stopwatch();
						Controller.quickSort(nTrips, ascX);
						double time = timer.elapsedTime();
						
						for (VOTrip trip : nTrips) 
						{
							System.out.println( trip );
						}
						System.out.println( "El tiempo (en milisegundos) que tom� el algoritmo de ordenamiento fue de: " + time );
					}
					else {
						System.out.println("Primero debe generar una muestra");
					}
				}
				else {
					System.out.println("Debe ingresar 0 o 1");
					break;				
				}
				break;

			case 5:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar datos de viajes");
		System.out.println("2. Generar muestra con N viajes");
		System.out.println("3. Ordenar la muestra por el algoritmo MergeSort y cronometrar el tiempo que tarda");
		System.out.println("4. Ordenar la muestra por el algoritmo QuickSort y cronometrar el tiempo que tarda");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
