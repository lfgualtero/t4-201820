package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import model.data_structures.Stack;



public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	/**
	 * Stack de trips
	 */
	private Stack<VOTrip> tripsS;
	
	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------
	public DivvyTripsManager() {
		tripsS=new Stack<VOTrip>();
	}
	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	
	
	
	/**
	 * Da el tama;o actual del stack de viajes
	 * @return el tama;o actual del stack de viajes
	 */
	public int getTripsSSize() {
		return tripsS.size();
	}
	

	/**
	 * Carga de los viajes realizadas por las bicicletas, los  viajes realizados deben cargarse en una Pila(Stack) y en una Cola
	 * (Queue) en el orden que aparecen en el archivo.
	 * @param tripsFile ubicacion del archivo.
	 */
	@Override
	public void loadTrips(String tripsFile) {

		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		Stack <VOTrip> tripStackAUX= new Stack<>();
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			while ((line = br.readLine()) != null) { 

//				System.out.println(line);

				String[] datos = line.split(cvsSplitBy);

				int trip_id;
				Date start_time;
				Date end_time;
				int bikeid;
				int tripduration;
				int from_station_id;
				String from_station_name;
				int to_station_id;
				String to_station_name;
				String usertype;
				String gender;
				int birthyear;


				try {

					if(line.startsWith("\"")) 
					{
						String pattern;
						if(datos[1].length()>18) {
							 pattern="dd/MM/yy HH:mm:ss";
							pattern="dd/MM/yy HH:mm:ss";
						}else {
							 pattern="dd/MM/yy HH:mm";
							pattern="dd/MM/yy HH:mm";
						}
						
						SimpleDateFormat format = new SimpleDateFormat(pattern);
						
						trip_id=Integer.parseInt(datos[0].substring(1, datos[0].length()-1));
						start_time=format.parse(datos[1].substring(1, datos[1].length()-1));
						end_time=format.parse(datos[2].substring(1, datos[2].length()-1));
						bikeid=Integer.parseInt(datos[3].substring(1, datos[3].length()-1));
						tripduration=Integer.parseInt(datos[4].substring(1, datos[4].length()-1));
						from_station_id=Integer.parseInt(datos[5].substring(1, datos[5].length()-1));
						from_station_name=datos[6].substring(1, datos[6].length()-1);
						to_station_id=Integer.parseInt(datos[7].substring(1, datos[7].length()-1));
						to_station_name=datos[8].substring(1, datos[8].length()-1);
						usertype=datos[9].substring(1, datos[9].length()-1);


						if(datos[10].equals("\""+"\"") && datos[11].equals("\""+"\"")) {

							gender=" ";
							birthyear=0;

						}else if(datos[10].equals("\""+"\"")) {

							gender=" ";
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}else if(datos[11].equals("\""+"\"")) {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear=0 ;


						}else {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}

						tripsS.push(new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear));
						tripStackAUX.push(new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear));




					}else 
					{

						String pattern;
						if(datos[1].length()>16) {
							 pattern="dd/MM/yy HH:mm:ss";
							pattern="dd/MM/yy HH:mm:ss";
						}else {
							 pattern="dd/MM/yy HH:mm";
							pattern="dd/MM/yy HH:mm";
						}
						

						SimpleDateFormat format = new SimpleDateFormat(pattern);
						

						trip_id=Integer.parseInt(datos[0]);
						start_time=format.parse(datos[1]);
						end_time=format.parse(datos[2]);
						bikeid=Integer.parseInt(datos[3]);
						tripduration=Integer.parseInt(datos[4]);
						from_station_id=Integer.parseInt(datos[5]);
						from_station_name=datos[6];
						to_station_id=Integer.parseInt(datos[7]);
						to_station_name=datos[8];
						usertype=datos[9];



						if(datos.length==10)
						{
							gender=" ";
							birthyear=0;

						}
						else if(datos.length==11)
						{
							gender=datos[10];
							birthyear=0;

						}
						else 
						{
							gender=datos[10];
							birthyear=Integer.parseInt(datos[11]);

						}

						VOTrip v = new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear);
						tripsS.push(v);
						tripStackAUX.push(v);

					}

				} catch (NumberFormatException e) {

					e.printStackTrace();
				} catch (ParseException e) {
					

					e.printStackTrace();
				}

			}
			
						
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}



	/**
	 * 
	 * Usa la Cola creada para retornar un arreglo de �n� viajes.
	 * @param n es el numero de las estaciones que se quiere.
	 * @return Una muestra de �n� viajes
	 */
	
	public  VOTrip[] getNTrips ( int n) {

		VOTrip[] nuevoArreglo= new VOTrip[n];

		System.out.println(tripsS.size());
		
		for (int i = 0; i < n; i++) {
			nuevoArreglo[i]=tripsS.pop();
		}
		
		return nuevoArreglo;
	}
	
	

	/**
	 * 
	 * Usa	la	Pila para	retornar la	informaci�n del Trip	n�mero	�n�	que	termin� en	la estaci�n cuyo	id	entra	como	par�metro. (El	
	 * Trip 1	corresponde	al	 primero	 que	lleg� a	esa	estaci�n,	el	Trip	 2	el	 segundo	 que	lleg� a	esa
	 * estaci�n,	y	as� sucesivamente).
	 * @param n es el numero del Trip que se quiere. stationID es la estacion a la que llego el bike en ese trip
	 * @return La	informaci�n del Trip	n�mero	�n�	que	termin� en	la estaci�n cuyo	id	entra	como	par�metro
	 */

	@Override
	public VOTrip customerNumberN (int stationID, int n) {

		Iterator<VOTrip> t = tripsS.iterator(); 
		int contador = n;

		VOTrip trip = null;

		while(t.hasNext() && contador>0 ) {

			trip=t.next();

			if(trip.getTo_station_id() == stationID) {
				contador--;
			}
		}
		return trip;
	}


	@Override
	public void mergeSortTrips(VOTrip[] elementos, boolean asc) {
		Ordenador<VOTrip> o = new Ordenador<VOTrip>();
		o.ordenar(SortingAlgorithms.MERGESORT, asc, elementos);
	}



	@Override
	public void quickSortTrips(VOTrip[] elementos, boolean asc) {
		Ordenador<VOTrip> o = new Ordenador<VOTrip>();
		o.ordenar(SortingAlgorithms.QUICKSORT, asc, elementos);
	}
	
	public Stack<VOTrip> getTrips(){
		return tripsS;
	}



	




}
