package model.logic;

public class Ordenador<V extends Comparable<V>> {


	public void ordenar(SortingAlgorithms algoritmo, boolean ascendente, V[] elementos) {
		switch (algoritmo)
		{
		case QUICKSORT:
			quickSort(elementos, 0, elementos.length-1, ascendente);
			break;
		case MERGESORT:
			ordenarMergeSort(elementos, ascendente);
			break;
		}
	}

	private void ordenarMergeSort(V[] elementos, boolean ascendente) {
		mergeSortAux(elementos, 0, elementos.length-1, ascendente);
	}

	/*
	 * Metodo auxiliar para unir las mitades ordenadas
	 */
	@SuppressWarnings("unchecked")
	private void mergeA(V arr[], int leftIndex, int middleIndex, int rightIndex) 
	{ 
		// Find sizes of two subarrays to be merged 
		int n1 = middleIndex - leftIndex + 1; 
		int n2 = rightIndex - middleIndex; 

		/* Create temp arrays */
		Object L[] = new Object [n1]; 
		Object R[] = new Object [n2]; 

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i) 
			L[i] = arr[leftIndex + i]; 
		for (int j=0; j<n2; ++j) 
			R[j] = arr[middleIndex + 1+ j]; 


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays 
		int i = 0, j = 0; 

		// Initial index of merged subarry array 
		int k = leftIndex; 
		while (i < n1 && j < n2) 
		{ 
			V tempI = (V) L[i];
			V tempJ = (V) R[j];
			if (tempI.compareTo(tempJ) < 0) 
			{ 
				arr[k] = (V) L[i]; 
				i++; 
			} 
			else
			{ 
				arr[k] = (V) R[j]; 
				j++; 
			} 
			k++; 
		} 

		/* Copy remaining elements of L[] if any */
		while (i < n1) 
		{ 
			arr[k] = (V) L[i]; 
			i++; 
			k++; 
		} 

		/* Copy remaining elements of R[] if any */
		while (j < n2) 
		{ 
			arr[k] = (V) R[j]; 
			j++; 
			k++; 
		} 
	} 


	@SuppressWarnings("unchecked")
	private void mergeD(V arr[], int leftIndex, int middleIndex, int rightIndex) 
	{ 
		// Find sizes of two subarrays to be merged 
		int n1 = middleIndex - leftIndex + 1; 
		int n2 = rightIndex - middleIndex; 

		/* Create temp arrays */
		Object L[] = new Object [n1]; 
		Object R[] = new Object [n2]; 

		/*Copy data to temp arrays*/
		for (int i=0; i<n1; ++i) 
			L[i] = arr[leftIndex + i]; 
		for (int j=0; j<n2; ++j) 
			R[j] = arr[middleIndex + 1+ j]; 


		/* Merge the temp arrays */

		// Initial indexes of first and second subarrays 
		int i = 0, j = 0; 

		// Initial index of merged subarry array 
		int k = leftIndex; 
		while (i < n1 && j < n2) 
		{ 
			V tempI = (V) L[i];
			V tempJ = (V) R[j];
			if (tempI.compareTo(tempJ) > 0) 
			{ 
				arr[k] = (V) L[i]; 
				i++; 
			} 
			else
			{ 
				arr[k] = (V) R[j]; 
				j++; 
			} 
			k++; 
		} 

		/* Copy remaining elements of L[] if any */
		while (i < n1) 
		{ 
			arr[k] = (V) L[i]; 
			i++; 
			k++; 
		} 

		/* Copy remaining elements of R[] if any */
		while (j < n2) 
		{ 
			arr[k] = (V) R[j]; 
			j++; 
			k++; 
		} 
	} 

	
	/*
	 * Metodo principal 
	 */
	private void mergeSortAux(V arr[], int leftIndex, int rightIndex, boolean asc) 
	{ 
		if (leftIndex < rightIndex) 
		{ 
			// Encuentra el indice del medio
			int middleIndex = (leftIndex+rightIndex)/2; 

			// Ordena la primera y la segunda mitad 
			mergeSortAux(arr, leftIndex, middleIndex, asc); 
			mergeSortAux(arr , middleIndex+1, rightIndex, asc); 

			// Une las dos mitades despues de ordenarlas
			
			if(asc == true)
			mergeA(arr, leftIndex, middleIndex, rightIndex); 
			else
			mergeD(arr, leftIndex, middleIndex, rightIndex);
		} 
	} 


	public int partitionA(V[] arr, int left, int right)
	{
		int i = left, j = right;
		V tmp;
		V pivot = arr[(left + right) / 2];

		while (i <= j) {
			
			while (arr[i].compareTo(pivot)<0)
				i++;

			while (arr[j].compareTo(pivot)>0)
				j--;

			if (i <= j) {
				tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;

				i++;
				j--;
			}
		}
		return i;
	}
	
	public int partitionD(V[] arr, int left, int right)
	{
		int i = left, j = right;
		V tmp;
		V pivot = arr[(left + right) / 2];

		while (i <= j) {
			//    arr[i] > pivot
			while (arr[i].compareTo(pivot)>0)
				i++;
			//    arr[j] < pivot
			while (arr[j].compareTo(pivot)<0)
				j--;

			if (i <= j) {
				tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;

				i++;
				j--;
			}
		}
		return i;
	}

	void quickSort(V[] elementos, int left, int right, boolean ascendente) {

		if(ascendente) {
		
		int index = partitionA(elementos, left, right);

		if (left < index - 1)
			quickSort(elementos, left, index - 1, ascendente);

		if (index < right)
			quickSort(elementos, index, right, ascendente);
		
		}else {
			
			int index = partitionD(elementos, left, right);

			if (left < index - 1)
				quickSort(elementos, left, index - 1, ascendente);

			if (index < right)
				quickSort(elementos, index, right, ascendente);
			
		}

	}

}
